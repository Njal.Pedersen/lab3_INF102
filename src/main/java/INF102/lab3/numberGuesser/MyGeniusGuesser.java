package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

	@Override
    public int findNumber(RandomNumber number) {
        int lower = number.getLowerbound();
        int upper = number.getUpperbound();
        int answer = myGuess(number, lower, upper);
        return answer;
    }

    private int myGuess(RandomNumber number, int lower, int upper) {
        int middle = lower + (upper - lower) / 2;
        int res = number.guess(middle);
        if (res == 0) {
            return middle;
        } 
        else if (res > 0) {
            return myGuess(number, lower, middle);
        }
        else {
            return myGuess(number, middle, upper);
        }
    }

}
