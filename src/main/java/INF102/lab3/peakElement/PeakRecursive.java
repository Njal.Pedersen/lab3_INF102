package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        if (numbers.isEmpty()) {
            throw new IllegalArgumentException("Empty list");
        }

        int left = 0;
        int middle = 1;
        int right = 2;

        //edge cases
        if (numbers.get(0) > numbers.get(1)) {
            return numbers.get(0);
        } 
        if (numbers.get(numbers.size() - 1) > numbers.get(numbers.size() - 2)) {
            return numbers.get(numbers.size() - 1);
        }
        int answer = peakSearch(numbers, left, middle, right);
        if (answer == Integer.MIN_VALUE) {
            throw new IllegalArgumentException("No peak element");
        }
        return answer;
    }

    private int peakSearch(List<Integer> numbersList, int left, int middle, int right) {
        if (right == numbersList.size() - 1) {
            return Integer.MIN_VALUE;
        }
        if (isPeak(numbersList.get(left), numbersList.get(middle), numbersList.get(right))) {
            return numbersList.get(middle);
        } else {
            left++;
            middle++;
            right++;
            return peakSearch(numbersList, left, middle, right);
        }
        
    }   

    private boolean isPeak(int prev, int current, int next) {
        return current > prev && current > next;
    }

}
