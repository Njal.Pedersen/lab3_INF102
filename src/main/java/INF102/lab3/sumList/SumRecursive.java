package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        long su = calc(0, 0, list);
        return su;
    }

    private long calc(long su, int n, List<Long> list) {
        if (n >= list.size()) {
            return su;
        }
        su += list.get(n);
        n++;
        return calc(su, n, list);
    }
}
